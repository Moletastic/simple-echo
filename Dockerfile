FROM golang:1.13

WORKDIR /app

COPY go.mod go.sum ./

RUN go mod download

COPY . .

RUN go get -d -v

EXPOSE 80

RUN go build

CMD ["./simple-echo"]